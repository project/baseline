<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--

Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Title      : Baseline
Version    : 1.0
Released   : 20070730
Description: A three-column, fixed-width template ideal for 1024x768 resolution.

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body>
<div id="header">
	<div id="logo">
		<h1><a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"> <?php print $site_name; ?></a></h1>
		<h2>
		<?php if ($site_slogan): ?>
		<?php print $site_slogan; ?>
		<?php endif; ?>
		</h2>
	</div>
		<?php if(isset($search_box)) { print($search_box); } ?>
</div>
<?php if ($primary_links): ?> <div id="menu"> <?php print theme('links', $primary_links); ?> </div> <?php endif; ?>
<div id="page">
	<div id="content">
		<div id="feature" class="box-orange">
						<?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
						<?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
						<?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
						<?php print $help; ?>
						<?php print $messages; ?>
						<?php print $content; ?>
		</div>
	<?php if($cbc_1): ?>
		<div class="two-cols">	
		<div class="box-blue col-one">
		<?php print($cbc_1); ?>
		</div>
	<?php endif; ?>
	<?php if($cbc_2): ?>
		<div class="box-pink col-two">
		<?php print($cbc_2); ?>
		</div>
		</div>
	<?php endif; ?>
	</div>
	<?php if($sidebar_1): ?>
		<div id="sidebar" class="two-cols">
		<div class="col-one">
		<div class="box-blue">
		<?php print($sidebar_1);?>
		</div>
		</div>
	<?php endif; ?>
	<?php if($sidebar_2): ?>
		<div class="col-two">
		<div class="box-pink">
		<?php print($sidebar_2); ?>
		</div>
		</div>
	<?php endif; ?>
	</div>
	<div style="clear: both;">&nbsp;</div>
</div>
<div id="footer">
<?php if(isset($footer_message)) { print($footer_message); } ?>
<p id="legal">Copyright &copy; 2007 Simpletex. All Rights Reserved | Designed by <a href="http://www.freecsstemplates.org/">Free CSS Templates</a></p>
<p id="links">Ported to Drupal by <a href="http://grahana.wordpress.com">Pavan</a></p>
</div>
</body>
</html>