<?php

/*
 * Declare the available regions implemented by this engine.
 *
 * @return
 *    An array of regions.  The first array element will be used as the default region for themes.
 *    Each array element takes the format: variable_name => t('human readable name')
 */
function baseline_regions() {
  return array(
       'sidebar_1' => t('Sidebar 1'),
       'sidebar_2' => t('Sidebar 2'),
       'content' => t('Content'),
       'cbc_1' => t('Content Bottom Column 1'),
       'cbc_2' => t('Content Bottom Column 2'),
       'footer' => t('footer')
  );
}

